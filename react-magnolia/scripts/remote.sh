#!/bin/bash
export PUBLIC_URL="https://mgnl-spa-react.netlify.app"
export REACT_APP_NODE_NAME="/spa-remote"
export REACT_APP_MGNL_API_PAGES="https://mgnl-spa-react.netlify.app/api/delivery/pages"
export REACT_APP_MGNL_API_TEMPLATEDEFINITION="http://ec2-18-197-148-192.eu-central-1.compute.amazonaws.com:8080/.rest/templateDefinition/v1/"

npm run build