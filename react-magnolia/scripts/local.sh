#!/bin/bash
export PUBLIC_URL="/.resources/spa/webresources"
export REACT_APP_NODE_NAME="/spa-home"
export REACT_APP_MGNL_API_PAGES="/.rest/delivery/pages"
export REACT_APP_MGNL_API_TEMPLATEDEFINITION="/.rest/templateDefinition/v1/"

npm run build