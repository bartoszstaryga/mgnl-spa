#!/bin/bash
export PUBLIC_URL="http://localhost:8080/magnoliaAuthor/.resources/spa/webresources"
export REACT_APP_NODE_NAME="/spa-home"
export REACT_APP_MGNL_API_PAGES="http://localhost:8080/magnoliaAuthor/.rest/delivery/pages"
export REACT_APP_MGNL_API_TEMPLATEDEFINITION="http://localhost:8080/magnoliaAuthor/.rest/templateDefinition/v1/"

npm start