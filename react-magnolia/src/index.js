import React from 'react';
import ReactDOM from 'react-dom';
import { EditablePage, EditorContextHelper } from '@magnolia/react-editor';
import Home from './pages/Home';
import Text from './components/Text';
import List from './components/List';
import Item from './components/Item';

const config = {
  componentMappings: {
    'spa:pages/Home': Home,
    'spa:pages/Remote': Home,
    'spa:components/Text': Text,
    'spa:components/List': List,
    'spa:components/Item': Item,
  },
};

const node = process.env.REACT_APP_NODE_NAME;
const pagesApi = process.env.REACT_APP_MGNL_API_PAGES;
const templateDefinitionApi = process.env.REACT_APP_MGNL_API_TEMPLATEDEFINITION;

class App extends React.Component {
  state = {};

  async componentDidMount() {
    let templateDefinitions;
    const pageRes = await fetch(
      pagesApi + node + window.location.pathname.replace(new RegExp('(.*' + node + '|.html)', 'g'), '')
    );
    const page = await pageRes.json();
    if (EditorContextHelper.inEditor()) {
      const templateDefinitionsRes = await fetch(templateDefinitionApi + page['mgnl:template']);
      templateDefinitions = await templateDefinitionsRes.json();
    }
    this.setState({ page, templateDefinitions });
  }

  render() {
    const { page, templateDefinitions } = this.state;

    return (
      <div>
        <div>react-magnolia</div>
        {page && config && <EditablePage content={page} config={config} templateDefinitions={templateDefinitions} />}
      </div>
    );
  }
}

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
